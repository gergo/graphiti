/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.export.batik;

import org.eclipse.graphiti.ui.internal.services.GraphitiUiInternal;
import org.eclipse.swt.widgets.Display;

public class Util {

	private static Util util = new Util();

	public static Util getInstance() {
		return util;
	}
	public Display getDisplay() {
		return GraphitiUiInternal.getWorkbenchService().getShell().getDisplay();
	}

}
