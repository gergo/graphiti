/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mgorning - Bug 391523 - Revise getSelectionInfo...() in IToolBehaviorProvider
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.tb;

/**
 * @since 0.10
 */
public class AnchorSelectionInfoImpl extends ShapeSelectionInfoImpl implements IAnchorSelectionInfo {

}
