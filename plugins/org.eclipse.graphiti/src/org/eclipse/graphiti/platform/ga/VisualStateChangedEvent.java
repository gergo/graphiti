/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.platform.ga;

/**
 * The Class VisualStateChangedEvent.
 * 
 * @see IVisualStateChangeListener
 */
public class VisualStateChangedEvent {

	public IVisualState.Type changedField;

	public int oldValue;

	public int newValue;

	/**
	 * Creates a new {@link VisualStateChangedEvent}.
	 * 
	 * @param changedField
	 *            the changed field
	 * @param oldValue
	 *            the int old value
	 * @param newValue
	 *            the int new value
	 */
	public VisualStateChangedEvent(IVisualState.Type changedField, int oldValue, int newValue) {
		this.changedField = changedField;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

}
