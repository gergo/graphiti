/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.platform.ga;

/**
 * The Interface IVisualStateHolder provides access to an {@link IVisualState}.
 */
public interface IVisualStateHolder {

	// TODO
	// The visual state must be inherited from the parent, like the colors
	// => getVisualState() and getOwnVisualState()

	/**
	 * Returns the {@link IVisualState}. Must not be null.
	 * 
	 * @return The {@link IVisualState}
	 */
	IVisualState getVisualState();
}
