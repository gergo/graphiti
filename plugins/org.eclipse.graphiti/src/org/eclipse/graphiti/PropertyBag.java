/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 394801 - AddGraphicalRepresentation doesn't carry properties
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * The class PropertyBag.
 */
public class PropertyBag implements IPropertyBag {
	private HashMap<Object, Object> propertyMap;

	public Object getProperty(Object key) {
		return getPropertyMap().get(key);
	}

	public Object putProperty(Object key, Object value) {
		return getPropertyMap().put(key, value);
	}

	/**
	 * @since 0.10
	 */
	public List<Object> getPropertyKeys() {
		List<Object> result = new ArrayList<Object>();

		if (propertyMap != null) {
			Iterator<Object> iterator = propertyMap.keySet().iterator();
			while (iterator.hasNext()) {
				Object key = (Object) iterator.next();
				result.add(key);
			}
		}

		return result;
	}

	private HashMap<Object, Object> getPropertyMap() {
		if (this.propertyMap == null) {
			this.propertyMap = new HashMap<Object, Object>();
		}
		return this.propertyMap;
	}

}
