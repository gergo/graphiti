/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.palette;

import java.util.List;

/**
 * The Interface IPaletteCompartmentEntry.
 */
public interface IPaletteCompartmentEntry extends IPaletteEntry {

	/**
	 * Gets the tool entries.
	 * 
	 * @return the tool entries inside this compartment
	 */
	List<IToolEntry> getToolEntries();

	/**
	 * set the initial open state of the compartment.
	 * 
	 * @param initiallyOpen
	 *            the initially open
	 */
	void setInitiallyOpen(boolean initiallyOpen);

	/**
	 * provide the initial open state of the compartment.
	 * 
	 * @return the open state
	 */
	boolean isInitiallyOpen();
}
