/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.palette.impl;

import org.eclipse.graphiti.palette.IPaletteSeparatorEntry;

/**
 * A separator for the palette.
 */
public class PaletteSeparatorEntry extends AbstractPaletteEntry implements IPaletteSeparatorEntry {

	/**
	 * Creates a new {@link PaletteSeparatorEntry}.
	 */
	public PaletteSeparatorEntry() {
		super("", ""); //$NON-NLS-1$//$NON-NLS-2$
	}
}
