/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.palette.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.graphiti.palette.IPaletteCompartmentEntry;
import org.eclipse.graphiti.palette.IToolEntry;

/**
 * The Class PaletteCompartmentEntry creates a compartment entry which
 * visualises as a drawer in the palette containing multiple tools.
 */
public class PaletteCompartmentEntry extends AbstractPaletteEntry implements IPaletteCompartmentEntry {

	private List<IToolEntry> toolEntries = new ArrayList<IToolEntry>();

	private boolean initiallyOpen = true;

	/**
	 * Creates a new {@link PaletteCompartmentEntry}.
	 * 
	 * @param label
	 *            the text label
	 * @param iconId
	 *            the icon which is displayed
	 */
	public PaletteCompartmentEntry(String label, String iconId) {
		super(label, iconId);
	}

	/**
	 * Gets the tool entries.
	 * 
	 * @return the tools contained in the compartment
	 */
	public List<IToolEntry> getToolEntries() {
		return this.toolEntries;
	}

	/**
	 * adds a tool entry to the compartment.
	 * 
	 * @param toolEntry
	 *            the tool entry
	 */
	public void addToolEntry(IToolEntry toolEntry) {
		this.toolEntries.add(toolEntry);
	}

	public void setInitiallyOpen(boolean initiallyOpen) {
		this.initiallyOpen = initiallyOpen;
	}

	public boolean isInitiallyOpen() {
		return this.initiallyOpen;
	}

}
