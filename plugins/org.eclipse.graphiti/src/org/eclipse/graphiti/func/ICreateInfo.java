/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.func;

/**
 * The Interface ICreateInfo.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface ICreateInfo {

	/**
	 * Gets the create name.
	 * 
	 * @return name for UI representation
	 */
	String getCreateName();

	/**
	 * Gets the create description.
	 * 
	 * @return description for UI representation
	 */
	String getCreateDescription();

	/**
	 * Gets the create image id.
	 * 
	 * @return image for UI representation
	 */
	String getCreateImageId();

	/**
	 * Gets the create large image id.
	 * 
	 * @return large image for UI representation
	 */
	String getCreateLargeImageId();

}
