/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.func;

/**
 * The Interface IProposal.
 * 
 * Clients can use/extend {@link Proposal}.
 * 
 * @since 0.8
 */
public interface IProposal {
	String getText();

	Object getObject();
}
