/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context;

import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Shape;

/**
 * The Interface IMoveShapeContext.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface IMoveShapeContext extends IMoveContext, IPictogramElementContext, ITargetContext, ITargetConnectionContext {

	/**
	 * Gets the shape.
	 * 
	 * @return the shape
	 */
	Shape getShape();

	/**
	 * Gets the source container.
	 * 
	 * @return the source container
	 */
	ContainerShape getSourceContainer();

	/**
	 * Gets the delta x.
	 * 
	 * @return the delta x
	 */
	int getDeltaX();

	/**
	 * Gets the delta y.
	 * 
	 * @return the delta y
	 */
	int getDeltaY();
}