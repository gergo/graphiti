/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context;

import org.eclipse.graphiti.datatypes.ILocation;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * The Interface ICreateConnectionContext.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface ICreateConnectionContext extends IContext, IConnectionContext {

	/**
	 * Gets the source pictogram element.
	 * 
	 * @return the source pictogram element
	 */
	PictogramElement getSourcePictogramElement();

	/**
	 * Gets the target pictogram element.
	 * 
	 * @return the target pictogram element
	 */
	PictogramElement getTargetPictogramElement();

	/**
	 * @return the sourceLocation
	 * @since 0.8
	 */
	ILocation getSourceLocation();

	/**
	 * @return the targetLocation
	 * @since 0.8
	 */
	ILocation getTargetLocation();
}