/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context;

import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * The Interface ICustomContext.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface ICustomContext extends ILocationContext {

	/**
	 * Gets the pictogram elements.
	 * 
	 * @return the currently selected pictogram elements
	 */
	PictogramElement[] getPictogramElements();

	/**
	 * Gets the inner pictogram element.
	 * 
	 * @return the pictogram element at the mouse click position
	 */
	PictogramElement getInnerPictogramElement();

	/**
	 * Gets the inner graphics algorithm.
	 * 
	 * @return the graphics algorithm at the mouse click position
	 */
	GraphicsAlgorithm getInnerGraphicsAlgorithm();
}
