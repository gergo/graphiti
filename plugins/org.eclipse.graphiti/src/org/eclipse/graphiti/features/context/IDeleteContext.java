/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.features.context;

/**
 * The Interface IDeleteContext.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface IDeleteContext extends IPictogramElementContext {

	/**
	 * Returns the multiple delete information.
	 * 
	 * @return IMultiDeleteInfo in case of a multiple delete operation and null
	 *         in all other cases.
	 * @since 0.8
	 */
	IMultiDeleteInfo getMultiDeleteInfo();

}