/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti;

/**
 * The Enum DiagramScrollingBehavior.
 */
public enum DiagramScrollingBehavior {

	/**
	 * The GEF default behavior.
	 */
	GEF_DEFAULT,
	
	/**
	 * The scrollbars are always visible.
	 */
	SCROLLBARS_ALWAYS_VISIBLE
}
