/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE and others
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    Philip Alldredge - Bug 418676 - Undo is not disabled when canUndo is false for Palette features
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.internal.command;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

/**
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class GFPreparableCommand2 extends RecordingCommand {

	private Command command;

	public GFPreparableCommand2(TransactionalEditingDomain editingDomain, Command command) {
		super(editingDomain, command.getDescription());
		this.setCommand(command);
	}

	@Override
	public boolean canUndo() {
		return getCommand().canUndo() && super.canUndo();
	}

	@Override
	protected void doExecute() {
		getCommand().execute();
	}

	private void setCommand(Command command) {
		this.command = command;
	}

	public Command getCommand() {
		return command;
	}
}
