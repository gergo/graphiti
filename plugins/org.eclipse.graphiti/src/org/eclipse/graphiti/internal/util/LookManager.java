/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.internal.util;

import org.eclipse.graphiti.util.ILook;

/**
 * The Class LookManager.
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class LookManager {

	private static ILook currentLook;

	private static ILook defaultLook;

	private static ILook dynamicLook;

	/**
	 * Gets the look.
	 * 
	 * @return the look
	 */
	public static ILook getLook() {
		if (currentLook == null) {
			currentLook = getDefaultLook();
		}
		return currentLook;
	}

	public static void setDynamicLook(boolean dynamic) {
		if (dynamic) {
			currentLook = getDynamicLook();
		} else {
			currentLook = getDefaultLook();
		}
	}

	private static ILook getDefaultLook() {
		if (defaultLook == null) {
			defaultLook = new DefaultLook();
		}
		return defaultLook;
	}

	private static ILook getDynamicLook() {
		if (dynamicLook == null) {
			dynamicLook = new DynamicLook();
		}
		return dynamicLook;
	}
}
