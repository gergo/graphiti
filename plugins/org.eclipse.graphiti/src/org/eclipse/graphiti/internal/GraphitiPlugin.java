/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.internal;

import org.eclipse.core.runtime.Plugin;

/**
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class GraphitiPlugin extends Plugin {

	public static final String PLUGIN_ID = "org.eclipse.graphiti"; //$NON-NLS-1$
	private static GraphitiPlugin sInstance;

	public GraphitiPlugin() {
		super();
		sInstance = this;
	}

	public static GraphitiPlugin getDefault() {
		return sInstance;
	}

}
