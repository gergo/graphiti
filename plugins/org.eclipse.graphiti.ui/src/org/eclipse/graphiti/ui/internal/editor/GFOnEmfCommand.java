/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.editor;

import org.eclipse.emf.common.command.Command;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.internal.command.ICommand;

/**
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class GFOnEmfCommand implements ICommand {

	private Command emfCommand;
	private IFeatureProvider featureProvider;

	public GFOnEmfCommand(Command emfCom, IFeatureProvider fp) {
		this.emfCommand = emfCom;
		this.featureProvider = fp;
	}

	public boolean canExecute() {
		return emfCommand.canExecute();
	}

	public boolean canUndo() {
		return emfCommand.canUndo();
	}

	public boolean execute() {
		emfCommand.execute();
		return true;
	}

	public boolean undo() {
		emfCommand.undo();
		return true;
	}

	public String getDescription() {
		return emfCommand.getDescription();
	}

	public IFeatureProvider getFeatureProvider() {
		return featureProvider;
	}
}
