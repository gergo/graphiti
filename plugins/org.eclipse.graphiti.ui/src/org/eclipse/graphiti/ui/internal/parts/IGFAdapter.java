/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.parts;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.ui.model.IWorkbenchAdapter;
import org.eclipse.ui.model.IWorkbenchAdapter2;

/**
 * The Interface IGFAdapter.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public interface IGFAdapter extends IWorkbenchAdapter, IWorkbenchAdapter2 {

	/**
	 * Gets the pictogram element.
	 * 
	 * @param object
	 *            the object
	 * 
	 * @return the pictogram element
	 */
	PictogramElement getPictogramElement(Object object);

	/**
	 * Gets the business objects.
	 * 
	 * @param object
	 *            the object
	 * 
	 * @return the business objects
	 */
	EObject[] getBusinessObjects(Object object);
}
