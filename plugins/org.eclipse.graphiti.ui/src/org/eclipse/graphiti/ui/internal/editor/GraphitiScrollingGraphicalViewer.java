/*********************************************************************
* Copyright (c) 2005, 2021 SAP SE, Space Codesign
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    Bug 336488 - DiagramEditor API
*    pjpaulin - Bug 352120 - Now uses IDiagramContainerUI interface
*    fvelasco - Bug 323356 - Mouse-wheel support for scrolling and zooming
*    Hubert Guerard, mwenz - Bug 543847 - Add capability to select several PictogramElement from a selection of PictogramElement
*    mwenz - Bug 549963 - GraphitiScrollingGraphicalViewer does not accept non-EditPart setSelection references
*    Hubert Guerard - Bug 573243 - Perform bulk selection instead of individually selection
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.MouseWheelHandler;
import org.eclipse.gef.MouseWheelZoomHandler;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;
import org.eclipse.graphiti.ui.editor.DiagramBehavior;
import org.eclipse.graphiti.ui.internal.util.gef.MouseWheelHorizontalScrollHandler;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;

public class GraphitiScrollingGraphicalViewer extends ScrollingGraphicalViewer {

	private DiagramBehavior diagramBehavior;

	public GraphitiScrollingGraphicalViewer(DiagramBehavior diagramBehavior) {
		this.setDiagramBehavior(diagramBehavior);
		setProperty(MouseWheelHandler.KeyGenerator.getKey(SWT.MOD1), MouseWheelZoomHandler.SINGLETON);
		setProperty(MouseWheelHandler.KeyGenerator.getKey(SWT.MOD2), MouseWheelHorizontalScrollHandler.SINGLETON);
	}

	@Override
	public void select(EditPart editpart) {
		IToolBehaviorProvider tbp = getDiagramBehavior().getDiagramTypeProvider().getCurrentToolBehaviorProvider();
		boolean connectionSelectionEnabled = tbp.isConnectionSelectionEnabled();

		Object model = editpart.getModel();
		if (connectionSelectionEnabled || !(model instanceof Connection)) {
			if (model != null && model instanceof PictogramElement) {
				deselectAll();
				PictogramElement[] newSelection = tbp.getSelections((PictogramElement) model);
				if (newSelection != null) {
					List<EditPart> newEditPartSelections = new ArrayList<EditPart>();
					for (PictogramElement newSelectionElement : newSelection) {
						EditPart newEditPart = (EditPart) getEditPartRegistry().get(newSelectionElement);
						if (newEditPart != null) {
							newEditPartSelections.add(newEditPart);
						}
					}
					super.setSelection(new StructuredSelection(newEditPartSelections));
				}
			}
		}
	}

	@Override
	public void setSelection(ISelection newSelection) {
		IToolBehaviorProvider tbp = getDiagramBehavior().getDiagramTypeProvider().getCurrentToolBehaviorProvider();
		boolean connectionSelectionPossible = tbp.isConnectionSelectionEnabled();

		boolean change = false;
		List<Object> selectionList = new ArrayList<Object>();
		if (newSelection instanceof IStructuredSelection) {
			IStructuredSelection strSel = (IStructuredSelection) newSelection;
			for (int i = 0; i < strSel.toArray().length; i++) {
				Object object = strSel.toArray()[i];
				if (object instanceof EditPart) {
					EditPart editPart = (EditPart) object;
					Object modelObject = editPart.getModel();
					if (modelObject instanceof PictogramElement) {
						if (connectionSelectionPossible || !(modelObject instanceof Connection)) {
							PictogramElement[] models = tbp.getSelections((PictogramElement) modelObject);
							if (models != null) {
								for (PictogramElement model : models) {
									EditPart modelEditPart = (EditPart) getEditPartRegistry().get(model);
									if (modelEditPart != null) {
										if (!selectionList.contains(modelObject)) {
											selectionList.add(modelEditPart);
											change = true;
										}
									}
								}
							} else {
								if (tbp.isMultiSelectionEnabled() || selectionList.isEmpty()) {
									selectionList.add(editPart);
									change = true;
								}
							}
						}
					}
				}
			}
			if (!tbp.isMultiSelectionEnabled()) {
				deselectAll();
			}
		}
		if (change) {
			newSelection = new StructuredSelection(selectionList);
		} else {
			List<EditPart> filteredSelection = new ArrayList<>();
			for (Object object : selectionList) {
				if (object instanceof EditPart) {
					filteredSelection.add((EditPart) object);
				}
			}
			newSelection = new StructuredSelection(filteredSelection);
		}
		super.setSelection(newSelection);
	}

	@Override
	public void appendSelection(EditPart editpart) {
		IToolBehaviorProvider tbp = getDiagramBehavior().getDiagramTypeProvider().getCurrentToolBehaviorProvider();
		Object model = editpart.getModel();

		if (!tbp.isConnectionSelectionEnabled() && model instanceof Connection) {
			return;
		}

		if (!tbp.isMultiSelectionEnabled()) {
			deselectAll();
		}

		if (model instanceof PictogramElement) {
			PictogramElement[] pes = tbp.getSelections((PictogramElement) model);
			if (pes != null) {
				List<Object> parts = new ArrayList<>();
				for (PictogramElement pe : pes) {
					EditPart editPart = (EditPart) getEditPartRegistry().get(pe);
					if (editPart != null) {
						parts.add(editPart);
					}
				}
				List<Object> selections = new ArrayList<>();
				ISelection oldSelection = super.getSelection();
				if (oldSelection instanceof IStructuredSelection) {
					selections.addAll(((IStructuredSelection) oldSelection).toList());
				}
				selections.addAll(parts);
				super.setSelection(new StructuredSelection(selections));
			}
		}
	}

	private void setDiagramBehavior(DiagramBehavior diagramBehavior) {
		this.diagramBehavior = diagramBehavior;
	}

	protected DiagramBehavior getDiagramBehavior() {
		return diagramBehavior;
	}
}
