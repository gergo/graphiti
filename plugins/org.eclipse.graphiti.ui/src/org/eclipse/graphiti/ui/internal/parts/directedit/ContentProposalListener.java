/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.internal.parts.directedit;

import org.eclipse.graphiti.func.IProposal;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalListener;

/**
 *
 */
public class ContentProposalListener implements IContentProposalListener {

	private TextCellEditor textCellEditor;

	/**
	 * 
	 */
	public ContentProposalListener(TextCellEditor textCellEditor) {
		super();
		this.textCellEditor = textCellEditor;
	}

	public void proposalAccepted(IContentProposal proposal) {
		if (proposal instanceof ContentProposal) {
			IProposal acceptedProposal = ((ContentProposal) proposal).getProposal();
			if (acceptedProposal != null) {
				textCellEditor.setAcceptedProposal(acceptedProposal);
			}
		}
	}
}
