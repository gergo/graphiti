/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.examples.common.util.uiprovider;

import org.eclipse.graphiti.examples.common.util.Util;

/**
 * A simple container, which just wraps two objects. Its main purpose is to overwrite equals() and hashCode() accordingly.
 */
public class TwoObjectsContainer {

	private Object _one;

	private Object _two;

	public TwoObjectsContainer(Object one, Object two) {
		_one = one;
		_two = two;
	}

	public final Object getOne() {
		return _one;
	}

	public final Object getTwo() {
		return _two;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) // quick check
			return true;
		if (!(o instanceof TwoObjectsContainer))
			return false;
		TwoObjectsContainer other = (TwoObjectsContainer) o;
		if (Util.equalsWithNull(other.getOne(), getOne()) && Util.equalsWithNull(other.getTwo(), getTwo()))
			return true;
		return false;
	}

	@Override
	public int hashCode() {
		int hashCode = 0;
		if (getOne() != null)
			hashCode ^= getOne().hashCode();
		if (getTwo() != null)
			hashCode ^= getTwo().hashCode();
		return hashCode;
	}

	@Override
	public String toString() {
		return getClass().getName() + "[" + getOne() + ", " + getTwo() + "]"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
}
