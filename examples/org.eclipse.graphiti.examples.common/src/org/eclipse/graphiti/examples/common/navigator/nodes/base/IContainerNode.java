/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.examples.common.navigator.nodes.base;

import org.eclipse.swt.graphics.Image;

/**
 * The Interface IContainerNode.
 */
public interface IContainerNode {

	/**
	 * Gets the parent.
	 * 
	 * @return the parent
	 */
	Object getParent();

	/**
	 * Gets the children.
	 * 
	 * @return the children
	 */
	Object[] getChildren();

	/**
	 * Checks for children.
	 * 
	 * @return true, if successful
	 */
	boolean hasChildren();

	/**
	 * Gets the text.
	 * 
	 * @return the text
	 */
	String getText();

	Image getImage();
}