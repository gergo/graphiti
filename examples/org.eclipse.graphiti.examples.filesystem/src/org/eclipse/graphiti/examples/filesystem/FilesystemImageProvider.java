/*********************************************************************
* Copyright (c) 2012, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.examples.filesystem;

import org.eclipse.graphiti.ui.platform.AbstractImageProvider;

public class FilesystemImageProvider extends AbstractImageProvider {

	protected static final String PREFIX = "org.eclipse.graphiti.examples.filesystem."; //$NON-NLS-1$

	public static final String IMG_CREATE_FILE = PREFIX + "createFile"; //$NON-NLS-1$
	public static final String IMG_DELETE_FILE = PREFIX + "deleteFile"; //$NON-NLS-1$

	@Override
	protected void addAvailableImages() {
		addImageFilePath(IMG_CREATE_FILE, "icons/create_file.png"); //$NON-NLS-1$
		addImageFilePath(IMG_DELETE_FILE, "icons/delete_file.png"); //$NON-NLS-1$
	}
}
