/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    Henrik Rentz-Reichert - mwenz - Bug 376544 - bug in re-connecting a connection with identical start and end anchor
*    mwenz - Bug 394801 - AddGraphicalRepresentation doesn't carry properties
*    mwenz - Bug 421754 - Absolute position of active Shape nested in inactive ContainerShape is calculated incorrectly
*    mwenz - Bug 421813 - Relative position to diagram of active Shape nested in inactive ContainerShape is calculated incorrectly
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.tests;

import org.eclipse.graphiti.tests.cases.CreateServiceTest;
import org.eclipse.graphiti.tests.cases.DefaultFeaturesTest;
import org.eclipse.graphiti.tests.cases.FeatureParametersTest;
import org.eclipse.graphiti.tests.cases.GaServiceTest;
import org.eclipse.graphiti.tests.cases.LinkServiceTest;
import org.eclipse.graphiti.tests.cases.PeServiceTest;
import org.eclipse.graphiti.tests.cases.ReconnectionTest;
import org.eclipse.graphiti.tests.internal.services.impl.PeServiceImplTest;
import org.eclipse.graphiti.tests.tb.DefaultToolBehaviorProviderTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ DefaultFeaturesTest.class, GaServiceTest.class, LinkServiceTest.class, PeServiceTest.class,
		CreateServiceTest.class, ReconnectionTest.class, FeatureParametersTest.class,
		DefaultToolBehaviorProviderTest.class, PeServiceImplTest.class })
public class AllTests {

}
