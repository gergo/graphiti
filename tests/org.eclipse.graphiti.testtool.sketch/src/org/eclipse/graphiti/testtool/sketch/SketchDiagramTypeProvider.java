/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.testtool.sketch;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;
import org.eclipse.graphiti.features.ConfigurableFeatureProviderWrapper;
import org.eclipse.graphiti.platform.ga.IGraphicsAlgorithmRendererFactory;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;

/**
 * The Class SketchDiagramTypeProvider.
 */
public class SketchDiagramTypeProvider extends AbstractDiagramTypeProvider {

	private IToolBehaviorProvider[] availableToolBehaviorProviders;

	private IGraphicsAlgorithmRendererFactory factory;

	/**
	 * Instantiates a new sketch diagram type provider.
	 */
	public SketchDiagramTypeProvider() {
		super();
		setFeatureProvider(new ConfigurableFeatureProviderWrapper(new SketchFeatureProvider(this)));
	}

	@Override
	public IToolBehaviorProvider[] getAvailableToolBehaviorProviders() {
		if (availableToolBehaviorProviders == null) {
			availableToolBehaviorProviders = new IToolBehaviorProvider[] { new SketchToolBehavior(this),
					new SketchViewerModeToolBehavior(this) };
		}
		return availableToolBehaviorProviders;
	}

	@Override
	public IGraphicsAlgorithmRendererFactory getGraphicsAlgorithmRendererFactory() {
		if (factory == null) {
			factory = new SketchGraphicsAlgorithmRendererFactory();
		}
		return factory;
	}
}
