/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.testtool.sketch.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.func.AbstractProposalSupport;
import org.eclipse.graphiti.func.IProposal;
import org.eclipse.graphiti.func.IProposalSupport;
import org.eclipse.graphiti.func.Proposal;
import org.eclipse.graphiti.testtool.sketch.SketchUtil;

/**
 * The Class SketchLabelDirectEditingFeature.
 */
public class SketchTextProposalDirectEditingFeature extends SketchTextDirectEditingFeature {

	private IProposalSupport proposalSupport = null;

	/**
	 * Instantiates a new sketch label direct editing feature.
	 * 
	 * @param fp
	 *            the fp
	 */
	public SketchTextProposalDirectEditingFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public IProposalSupport getProposalSupport() {
		if (proposalSupport == null) {
			proposalSupport = new AbstractProposalSupport() {

				@Override
				public void setValue(String value, IProposal proposal, IDirectEditingContext context) {
					String text = value;
					if (proposal != null && proposal.getText() != null) {
						text = proposal.getText();
					}
					SketchUtil.setCurrentLabelValue(context.getPictogramElement(), text);
				}

				@Override
				public IProposal[] getValueProposals(String value, int caretPos, IDirectEditingContext context) {
					return new IProposal[] {
							new Proposal("proposal_1", context.getPictogramElement()), new Proposal("proposal_2", context.getGraphicsAlgorithm()) }; //$NON-NLS-1$ //$NON-NLS-2$
				}
			};
		}

		return proposalSupport;
	}
}
