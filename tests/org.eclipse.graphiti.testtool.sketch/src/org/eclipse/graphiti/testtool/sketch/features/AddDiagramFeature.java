/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.testtool.sketch.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;

/**
 * The Class AddDiagramFeature.
 */
public class AddDiagramFeature extends AbstractAddShapeFeature {

	/**
	 * Instantiates a new adds the diagram feature.
	 * 
	 * @param fp
	 *            the fp
	 */
	public AddDiagramFeature(IFeatureProvider fp) {
		super(fp);
	}

	public PictogramElement add(IAddContext context) {
		if (context.getNewObject() instanceof Shape) {
			Shape shape = (Shape) context.getNewObject();
			context.getTargetContainer().getChildren().add(shape);
		}
		return null;
	}

	public boolean canAdd(IAddContext context) {
		if (context.getTargetContainer() != null && context.getNewObject() instanceof Diagram) {
			return true;
		}
		return false;
	}

}
