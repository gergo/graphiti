/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.testtool.sketch;

import org.eclipse.graphiti.features.FeatureCheckerAdapter;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.testtool.sketch.features.SwitchModeFeature;

public class ViewerModeChecker extends FeatureCheckerAdapter {

	public ViewerModeChecker() {
		super(false);
	}

	@Override
	public boolean allow(IFeature feature, IContext context) {
		if (feature instanceof SwitchModeFeature) {
			if (context instanceof CustomContext) {
				CustomContext cc = (CustomContext) context;
				PictogramElement[] pes = cc.getPictogramElements();
				return pes[0] instanceof Diagram;
			}
		}
		return false;
	}

	@Override
	public boolean allowCustomFeatures(ICustomContext context) {
		return true;
	}
}
