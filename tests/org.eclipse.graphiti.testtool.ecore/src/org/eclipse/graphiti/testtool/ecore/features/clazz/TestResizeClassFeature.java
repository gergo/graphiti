/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.testtool.ecore.features.clazz;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.impl.DefaultResizeShapeFeature;

/**
 * The Class TestResizeClassFeature.
 */
public class TestResizeClassFeature extends DefaultResizeShapeFeature {

	/**
	 * Instantiates a new test resize class feature.
	 * 
	 * @param fp
	 *            the fp
	 */
	public TestResizeClassFeature(IFeatureProvider fp) {
		super(fp);
	}
}
