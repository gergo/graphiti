/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.testtool.ecore;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;

/**
 * The Class TestDiagramTypeProvider.
 */
public class TestDiagramTypeProvider extends AbstractDiagramTypeProvider {

	private IToolBehaviorProvider[] availableToolBehaviorProviders;

	/**
	 * Instantiates a new test diagram type provider.
	 */
	public TestDiagramTypeProvider() {
		super();
		setFeatureProvider(new TestFeatureProvider(this));
	}

	/**
	 * definition of a new diagram type.
	 * 
	 * @return the available tool behavior providers
	 */

	@Override
	public IToolBehaviorProvider[] getAvailableToolBehaviorProviders() {
		if (availableToolBehaviorProviders == null) {
			availableToolBehaviorProviders = new IToolBehaviorProvider[] { new TestToolBehavior(this) };
		}
		return availableToolBehaviorProviders;
	}
}
