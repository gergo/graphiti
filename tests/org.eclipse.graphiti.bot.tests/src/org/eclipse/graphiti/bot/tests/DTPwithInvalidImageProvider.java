/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    mwenz - Bug 352709 - invalid image provider id crashes diagram editor
*    mwenz - Bug 421626 - Moved from ui.test to bot.test plugin
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.bot.tests;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;

public class DTPwithInvalidImageProvider extends AbstractDiagramTypeProvider {

	public DTPwithInvalidImageProvider() {
		super();
	}
}
