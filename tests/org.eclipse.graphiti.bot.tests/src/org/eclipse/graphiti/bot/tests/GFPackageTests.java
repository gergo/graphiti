/*********************************************************************
* Copyright (c) 2005, 2022 SAP SE and others
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 363539 - Enabled feature delegation via IDiagramEditor.execute method
*    mgorning - Bug 371671 - addGraphicalRepresentation returns null in dark mode
*    mwenz - Felix Velasco - Bug 374918 - Let default paste use LocalSelectionTransfer
*    fvelasco - Bug 396247 - ImageDescriptor changes
*    pjpaulin - Bug 352120 - Now uses IDiagramContainerUI interface
*    mwenz - Bug 370888 - API Access to export and print
*    mwenz - Bug 580392 - Replace EasyMock with Mockito
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.bot.tests;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.bot.tests.features.DefaultCopyFeature;
import org.eclipse.graphiti.bot.tests.util.ITestConstants;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.ConfigurableFeatureProviderWrapper;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IMappingProvider;
import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ICopyContext;
import org.eclipse.graphiti.features.context.IPasteContext;
import org.eclipse.graphiti.features.context.IReconnectionContext;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.context.impl.AreaAnchorContext;
import org.eclipse.graphiti.features.context.impl.AreaContext;
import org.eclipse.graphiti.features.context.impl.CopyContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.features.context.impl.PasteContext;
import org.eclipse.graphiti.features.context.impl.ReconnectionContext;
import org.eclipse.graphiti.features.context.impl.UpdateContext;
import org.eclipse.graphiti.features.impl.AbstractAddFeature;
import org.eclipse.graphiti.features.impl.AbstractFeature;
import org.eclipse.graphiti.features.impl.DefaultMoveAnchorFeature;
import org.eclipse.graphiti.features.impl.DefaultReconnectionFeature;
import org.eclipse.graphiti.internal.features.context.impl.base.DefaultContext;
import org.eclipse.graphiti.mm.Property;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.BoxRelativeAnchor;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.FixPointAnchor;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.PictogramLink;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.pattern.mapping.data.ImageDataMapping;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.testtool.ecore.TestDiagramTypeProvider;
import org.eclipse.graphiti.testtool.sketch.SketchDiagramTypeProvider;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.graphiti.ui.editor.IDiagramContainerUI;
import org.eclipse.graphiti.ui.features.AbstractPasteFeature;
import org.eclipse.graphiti.ui.features.DefaultFeatureProvider;
import org.eclipse.graphiti.ui.internal.GraphitiUIPlugin;
import org.eclipse.graphiti.ui.internal.editor.GFMarqueeSelectionTool;
import org.eclipse.graphiti.ui.internal.util.clipboard.ModelClipboard;
import org.eclipse.graphiti.ui.services.GraphitiUi;
import org.eclipse.graphiti.util.LocationInfo;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.junit.Test;

// Check whether this test makes sense at all...
@SuppressWarnings("restriction")
public class GFPackageTests extends AbstractGFTests {

	public GFPackageTests() {
		super();
	}

	@Test
	public void testDarkFeatureProcessing() throws Exception {
		String[] providerIds = GraphitiUi.getExtensionManager().getDiagramTypeProviderIds(
				ITestConstants.DIAGRAM_TYPE_ID_SKETCH);
		assertTrue("no dtp for sketch diagram type available", providerIds.length > 0);
		if (providerIds.length > 0) {
			final Diagram d = createDiagram(ITestConstants.DIAGRAM_TYPE_ID_SKETCH, "diagram");
			IDiagramTypeProvider dtp = GraphitiUi.getExtensionManager().createDiagramTypeProvider(d, providerIds[0]);
			assertNotNull("dtp couldn't be instantiated", dtp);

			// Bug 363539: assure that feature execution in dummy editor works
			final boolean[] canExecuteCalled = { false };
			final boolean[] executeCalled = { false };
			dtp.getDiagramBehavior().executeFeature(new AbstractFeature(dtp.getFeatureProvider()) {
				public boolean canExecute(IContext context) {
					canExecuteCalled[0] = true;
					return true;
				}

				public void execute(IContext context) {
					executeCalled[0] = true;
				}
			}, new DefaultContext());
			assertTrue(canExecuteCalled[0]);
			assertTrue(executeCalled[0]);

			// Bug 371671 - addGraphicalRepresentation returns null in dark mode
			AbstractAddFeature addFeature = new AbstractAddFeature(dtp.getFeatureProvider()) {

				public boolean canAdd(IAddContext context) {
					return true;
				}

				public PictogramElement add(IAddContext context) {
					Shape shape = Graphiti.getPeCreateService().createShape(d, true);
					return shape;
				}
			};

			Object result = dtp.getDiagramBehavior().executeFeature(addFeature, new AddContext());
			assertTrue(result instanceof Shape);
		}
	}

	@Test
	public void testGraphitiUi() throws Exception {
		String id = DiagramEditor.DIAGRAM_EDITOR_ID;
		assertNotNull(id);
		assertTrue(!("".equals(id)));
		assertNotNull(GraphitiUIPlugin.getDefault());
	}

	@Test
	public void testGraphitiUiInternalEditor() throws Exception {
		new GFMarqueeSelectionTool();
	}

	@Test
	public void testGraphitiFeatures() throws Exception {

		SketchDiagramTypeProvider mySketchDiagramTypeProvider = new SketchDiagramTypeProvider();
		IFeatureProvider provider = mySketchDiagramTypeProvider.getFeatureProvider();
		assertTrue(provider instanceof ConfigurableFeatureProviderWrapper);
		ConfigurableFeatureProviderWrapper myConfigurableFeatureProviderWrapper = (ConfigurableFeatureProviderWrapper) provider;

		// test canAdd
		AreaContext areaContext = new AreaContext();
		areaContext.setLocation(10, 20);
		final Diagram diagram = createDiagram("testPackageOrgEclipseGraphitiFeatures");
		Object value = new Object();
		AddContext addContext = new AddContext(areaContext, value);
		addContext.setTargetContainer(diagram);
		assertTrue(myConfigurableFeatureProviderWrapper.canAdd(addContext).toBoolean());
		addContext.setTargetContainer(null);
		assertFalse(myConfigurableFeatureProviderWrapper.canAdd(addContext).toBoolean());

		// test getAddFeature
		addContext.setTargetContainer(diagram);
		assertNotNull(myConfigurableFeatureProviderWrapper.getAddFeature(addContext));

		// test canLayout
		IDiagramContainerUI diagramEditor = openDiagramEditor(ITestConstants.DIAGRAM_TYPE_ID_ECORE);
		PictogramElement pe = getPictogramElement(diagramEditor);
		LayoutContext layoutContext = new LayoutContext(pe);
		assertFalse(myConfigurableFeatureProviderWrapper.canLayout(layoutContext).toBoolean());
		page.closeActiveEditor();
	}

	@Test
	public void testGraphitiFeaturesContext() throws Exception {


		String s = null;
		TestDiagramTypeProvider myDiagramTypeProvider = new TestDiagramTypeProvider();
		final IDiagramContainerUI diagramEditor = openDiagramEditor(ITestConstants.DIAGRAM_TYPE_ID_ECORE);
		final Diagram diagram = diagramEditor.getDiagramTypeProvider().getDiagram();
		myDiagramTypeProvider.init(diagram, diagramEditor.getDiagramBehavior());
		PictogramElement pe = getPictogramElement(diagramEditor);
		EList<Shape> shapes = diagram.getChildren();
		assertNotNull(pe);
		PictogramElement[] pes = new PictogramElement[] { pe };

		final DefaultCopyFeature myDefaultCopyFeature = new DefaultCopyFeature(
				myDiagramTypeProvider.getFeatureProvider());
		final ICopyContext copyContext = new CopyContext(pes);
		assertEquals(true, myDefaultCopyFeature.canExecute(copyContext));

		syncExec(new VoidResult() {
			public void run() {
				myDefaultCopyFeature.execute(copyContext);
			}
		});

		s = null;
		s = myDefaultCopyFeature.getName();
		assertNotNull(s);
		assertFalse("".equals(s));
		final MyPasteFeature myPasteFeature = new MyPasteFeature(myDiagramTypeProvider.getFeatureProvider(), diagram);
		final PasteContext pasteContext = new PasteContext(pes);
		pasteContext.setLocation(100, 200);

		syncExec(new VoidResult() {
			public void run() {
				assertEquals(true, myPasteFeature.canExecute(pasteContext));

				myPasteFeature.execute(pasteContext);
				diagramEditor.doSave(null);
				// duplicated and pasted objects are not equal with original
				// objects
				assertFalse(myPasteFeature.isEqual());
			}
		});

		s = null;
		s = myPasteFeature.getName();
		assertNotNull(s);
		assertFalse("".equals(s));

		// test ModelClipboard to paste a root element (parent = null)
		final Diagram dia = myDiagramTypeProvider.getDiagram();
		final EObject[] objs = new EObject[] { dia };
		syncExec(new VoidResult() {
			public void run() {
				ModelClipboard.getDefault().setContent(objs);
				Collection<EObject> copy = ModelClipboard.getDefault().duplicateAndPaste(null,
						ed.getTransactionalEditingDomain());
				diagramEditor.doSave(null);
				assertTrue(!copy.isEmpty() && !copy.contains(dia));
			}
		});

		DefaultMoveAnchorFeature myDefaultMoveAnchorFeature = new DefaultMoveAnchorFeature(
				myDiagramTypeProvider.getFeatureProvider());

		GraphicsAlgorithm graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);

		FixPointAnchor fixPointAnchorMock = mock(FixPointAnchor.class);
		when(fixPointAnchorMock.getGraphicsAlgorithm()).thenReturn(graphicsAlgorithmMock);

		AreaAnchorContext myAreaAnchorContext = new AreaAnchorContext(fixPointAnchorMock);
		assertEquals(true, myDefaultMoveAnchorFeature.canMoveAnchor(myAreaAnchorContext));

		myAreaAnchorContext.setLocation(10, 20);
		myDefaultMoveAnchorFeature.canExecute(myAreaAnchorContext);
		myDefaultMoveAnchorFeature.execute(myAreaAnchorContext);

		AnchorContainer myAnchorContainer = mock(AnchorContainer.class);
		when(myAnchorContainer.getGraphicsAlgorithm()).thenReturn(graphicsAlgorithmMock);

		BoxRelativeAnchor boxRelativeAnchor = mock(BoxRelativeAnchor.class);
		when(boxRelativeAnchor.getParent()).thenReturn(myAnchorContainer);
		when(boxRelativeAnchor.getGraphicsAlgorithm()).thenReturn(graphicsAlgorithmMock);

		myAreaAnchorContext = new AreaAnchorContext(boxRelativeAnchor);
		myAreaAnchorContext.setLocation(10, 20);

		myDefaultMoveAnchorFeature.moveAnchor(myAreaAnchorContext);

		s = null;
		s = myDefaultMoveAnchorFeature.getName();
		assertNotNull(s);
		assertFalse("".equals(s));

		DefaultReconnectionFeature myDefaultReconnectionFeature = new DefaultReconnectionFeature(
				myDiagramTypeProvider.getFeatureProvider());

		Anchor anchorMock = mock(Anchor.class);
		when(anchorMock.getParent()).thenReturn(shapes.get(0));

		org.eclipse.graphiti.mm.pictograms.Connection connectionMock = mock(
				org.eclipse.graphiti.mm.pictograms.Connection.class);
		when(connectionMock.getEnd()).thenReturn(boxRelativeAnchor);
		when(connectionMock.getStart()).thenReturn(anchorMock);

		IReconnectionContext myReconnectionContext = new ReconnectionContext(connectionMock, anchorMock, anchorMock,
				null);
		myReconnectionContext.setTargetPictogramElement(pe);

		assertTrue(myDefaultReconnectionFeature.canExecute(myReconnectionContext));

		myDefaultReconnectionFeature.reconnect(myReconnectionContext);

		s = null;
		s = myDefaultReconnectionFeature.getName();
		assertNotNull(s);
		assertFalse("".equals(s));

		DefaultFeatureProvider myDefaultFeatureProvider = new DefaultFeatureProvider(myDiagramTypeProvider);
		IUpdateContext context = new UpdateContext(null);
		IUpdateFeature updateFeature = myDefaultFeatureProvider.getUpdateFeature(context);
		final PictogramLink linkForPictogramElement = Graphiti.getLinkService().getLinkForPictogramElement(pe);
		if (linkForPictogramElement != null) {
			final EList<EObject> businessObject = linkForPictogramElement.getBusinessObjects();
			if (businessObject != null && !businessObject.isEmpty()) {
				executeInRecordingCommandInUIThread(diagramEditor.getDiagramBehavior(), new Runnable() {
					public void run() {
						businessObject.removeAll(businessObject);
					}
				});
			}
		}
		context = new UpdateContext(pe);
		try {
			updateFeature.update(context);
		} catch (Exception e) {
			// ignore
		}

		page.closeActiveEditor();
	}

	@Test
	public void testGraphitiPasteTwoDiagrams() throws Exception {
		TestDiagramTypeProvider myDiagramTypeProvider = new TestDiagramTypeProvider();
		final IDiagramContainerUI diagramEditor = openDiagramEditor(ITestConstants.DIAGRAM_TYPE_ID_ECORE);
		final Diagram diagram = diagramEditor.getDiagramTypeProvider().getDiagram();
		myDiagramTypeProvider.init(diagram, diagramEditor.getDiagramBehavior());
		PictogramElement pe = getPictogramElement(diagramEditor);
		assertNotNull(pe);
		PictogramElement[] pes = new PictogramElement[] { pe };
		syncExec(new VoidResult() {
			public void run() {
				diagramEditor.doSave(null);
			}
		});

		final DefaultCopyFeature myDefaultCopyFeature = new DefaultCopyFeature(
				myDiagramTypeProvider.getFeatureProvider());
		final ICopyContext copyContext = new CopyContext(pes);
		assertEquals(true, myDefaultCopyFeature.canExecute(copyContext));

		syncExec(new VoidResult() {
			public void run() {
				myDefaultCopyFeature.execute(copyContext);
			}
		});

		// Test paste in a second diagram
		TestDiagramTypeProvider myDiagramTypeProvider2 = new TestDiagramTypeProvider();
		final IDiagramContainerUI diagramEditor2 = openDiagramEditor(ITestConstants.DIAGRAM_TYPE_ID_ECORE, "xmi", "diagram2");
		final Diagram diagram2 = diagramEditor2.getDiagramTypeProvider().getDiagram();
		myDiagramTypeProvider2.init(diagram2, diagramEditor2.getDiagramBehavior());
		final MyPasteFeature myPasteFeature = new MyPasteFeature(myDiagramTypeProvider2.getFeatureProvider(), diagram2);
		final PasteContext pasteContext = new PasteContext(pes);
		pasteContext.setLocation(100, 200);

		syncExec(new VoidResult() {
			public void run() {
				assertEquals(true, myPasteFeature.canExecute(pasteContext));

				myPasteFeature.execute(pasteContext);
				// duplicated and pasted objects are not equal with original
				// objects
				assertFalse(myPasteFeature.isEqual());
			}
		});

		syncExec(new VoidResult() {
			public void run() {
				diagramEditor2.doSave(null);
			}
		});
		page.closeAllEditors();
	}

	private PictogramElement getPictogramElement(IDiagramContainerUI diagramEditor) {

		Diagram diagram = diagramEditor.getDiagramTypeProvider().getDiagram();
		addClassesAndReferencesToDiagram(diagramEditor);
		EList<Shape> shapes = diagram.getChildren();
		PictogramLink link;
		link = shapes.get(0).getLink();
		PictogramElement pe;
		pe = link.getPictogramElement();
		return pe;
	}

	private void addClassesAndReferencesToDiagram(final IDiagramContainerUI diagramEditor) {
		syncExec(new VoidResult() {
			public void run() {
				IDiagramTypeProvider diagramTypeProvider = diagramEditor.getDiagramTypeProvider();
				final IFeatureProvider fp = diagramTypeProvider.getFeatureProvider();
				final Diagram currentDiagram = diagramTypeProvider.getDiagram();
				executeInRecordingCommand(diagramEditor.getDiagramBehavior(), new Runnable() {
					public void run() {
						addClassesAndReferenceToDiagram(fp, currentDiagram, -100, -100, "Connection", -700, -200,
								"ConnectionDecorator");
					}
				});
			}
		});
	}

	// Should be moved to a unit test plugin for the graphiti pattern plugin
	@Test
	public void testMappingData() throws Exception {
		class MyImageDataMapping extends ImageDataMapping {
			public MyImageDataMapping(IMappingProvider mappingProvider) {
				super(mappingProvider);
			}
		}

		IMappingProvider mappingProviderMock = mock(IMappingProvider.class);

		MyImageDataMapping myImageDataMapping = new MyImageDataMapping(mappingProviderMock);

		PictogramLink pictogramLinkMock = mock(PictogramLink.class);

		myImageDataMapping.getImageId(pictogramLinkMock);

		myImageDataMapping.getUpdateWarning(pictogramLinkMock);
	}

	@Test
	public void testGraphitiUtil() throws Exception {
		Shape shapeMock = mock(Shape.class);

		GraphicsAlgorithm graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);

		LocationInfo myLocationInfo = new LocationInfo(shapeMock, graphicsAlgorithmMock);

		assertTrue(shapeMock.equals(myLocationInfo.getShape()));

		assertTrue(graphicsAlgorithmMock.equals(myLocationInfo.getGraphicsAlgorithm()));

		graphicsAlgorithmMock = mock(GraphicsAlgorithm.class);
		when(graphicsAlgorithmMock.getX()).thenReturn(10);
		when(graphicsAlgorithmMock.getY()).thenReturn(10);

		PictogramElement pictogramElementMock = mock(PictogramElement.class);
		when(pictogramElementMock.getGraphicsAlgorithm()).thenReturn(graphicsAlgorithmMock);

		shapeMock = mock(Shape.class);
		when(shapeMock.getGraphicsAlgorithm()).thenReturn(graphicsAlgorithmMock);
		EList<Shape> shapeList = new BasicEList<Shape>();
		shapeList.add(shapeMock);

		EList<org.eclipse.graphiti.mm.algorithms.styles.Point> points = new BasicEList<org.eclipse.graphiti.mm.algorithms.styles.Point>();

		ConnectionDecorator connectionDecoratorMock1 = mock(ConnectionDecorator.class);
		when(connectionDecoratorMock1.isLocationRelative()).thenReturn(Boolean.valueOf(true));
		when(connectionDecoratorMock1.getGraphicsAlgorithm()).thenReturn(graphicsAlgorithmMock);

		ConnectionDecorator connectionDecoratorMock2 = mock(ConnectionDecorator.class);
		when(connectionDecoratorMock2.isLocationRelative()).thenReturn(Boolean.valueOf(false));
		when(connectionDecoratorMock2.getGraphicsAlgorithm()).thenReturn(graphicsAlgorithmMock);

		EList<ConnectionDecorator> decorators = new BasicEList<ConnectionDecorator>();
		decorators.add(connectionDecoratorMock1);
		decorators.add(connectionDecoratorMock2);

		FreeFormConnection connectionMock = mock(FreeFormConnection.class);
		when(connectionMock.getBendpoints()).thenReturn(points);
		when(connectionMock.getConnectionDecorators()).thenReturn(decorators);
		when(connectionMock.getGraphicsAlgorithm()).thenReturn(graphicsAlgorithmMock);

		EList<org.eclipse.graphiti.mm.pictograms.Connection> connections = new BasicEList<org.eclipse.graphiti.mm.pictograms.Connection>();
		connections.add(connectionMock);

		Diagram diagramMock = mock(Diagram.class);
		when(diagramMock.getChildren()).thenReturn(shapeList);
		when(diagramMock.getConnections()).thenReturn(connections);

		decorators = new BasicEList<ConnectionDecorator>();
		decorators.add(connectionDecoratorMock2);

		connectionMock = mock(FreeFormConnection.class);
		when(connectionMock.getBendpoints()).thenReturn(points);
		when(connectionMock.getConnectionDecorators()).thenReturn(decorators);
		when(connectionMock.getGraphicsAlgorithm()).thenReturn(graphicsAlgorithmMock);

		connections = new BasicEList<org.eclipse.graphiti.mm.pictograms.Connection>();
		connections.add(connectionMock);

		diagramMock = mock(Diagram.class);
		when(diagramMock.getChildren()).thenReturn(shapeList);
		when(diagramMock.getConnections()).thenReturn(connections);

		final IDiagramContainerUI diagramEditor = openDiagramEditor(ITestConstants.DIAGRAM_TYPE_ID_ECORE);
		final Diagram diagram = diagramEditor.getDiagramTypeProvider().getDiagram();

		IDiagramTypeProvider diagramTypeProvider = diagramEditor.getDiagramTypeProvider();
		final IFeatureProvider fp = diagramTypeProvider.getFeatureProvider();
		final Diagram currentDiagram = diagramTypeProvider.getDiagram();
		executeInRecordingCommandInUIThread(diagramEditor.getDiagramBehavior(), new Runnable() {
			public void run() {
				addClassesAndReferenceToDiagram(fp, currentDiagram, -100, -100, "Connection", -700, -200,
						"ConnectionDecorator");
				moveClassShape(fp, currentDiagram, 300, 300, "Connection");
				getPeService().setPropertyValue(diagram, "Test", "test");
			}
		});
		Property property = getPeService().getProperty(diagram, "Test");
		assertTrue("test".equals(property.getValue()));

		pictogramElementMock = mock(PictogramElement.class);

		page.closeActiveEditor();
	}

	final class MyPasteFeature extends AbstractPasteFeature {

		private PictogramElement[] copy;
		private PictogramElement[] fromClipboard = new PictogramElement[1];
		private Diagram diagram;

		public MyPasteFeature(IFeatureProvider fp, Diagram diagram) {
			super(fp);
			this.diagram = diagram;
		}

		public boolean canPaste(IPasteContext context) {
			this.copy = context.getPictogramElements();
			if (this.copy.length == 1) {
				if (this.copy[0] instanceof ContainerShape) {
					return getFromClipboard().length > 0;
				}
			}
			return false;
		}

		public void paste(IPasteContext context) {
			Object[] obs = getCopiesFromClipBoard(this.diagram);
			assertTrue((obs != null && obs.length > 0 && obs[0] instanceof PictogramElement));
			assertEquals(100, context.getX());
			assertEquals(200, context.getY());
			for (int i = 0; i < obs.length; i++) {
				if (obs[i] instanceof PictogramElement) {
					this.fromClipboard[i] = (PictogramElement) obs[i];
				}
			}
		}

		public boolean isEqual() {
			boolean res = false;
			if (this.copy != null && this.fromClipboard != null) {
				res = getFeatureProvider().getDiagramTypeProvider().getCurrentToolBehaviorProvider()
						.equalsBusinessObjects(this.copy[0], this.fromClipboard[0]);
			}
			return res;
		}
	}
}
