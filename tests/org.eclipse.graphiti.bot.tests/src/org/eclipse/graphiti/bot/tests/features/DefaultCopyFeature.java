/*********************************************************************
* Copyright (c) 2005, 2019 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    SAP SE - initial API, implementation and documentation
*    mwenz - Bug 374918 - Let default paste use LocalSelectionTransfer
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.bot.tests.features;import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICopyContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.ui.features.AbstractCopyFeature;
;

/**
 * The Class DefaultCopyFeature.
 */
public class DefaultCopyFeature extends AbstractCopyFeature {

	/**
	 * The Constructor.
	 * 
	 * @param fp
	 *            the fp
	 */
	public DefaultCopyFeature(IFeatureProvider fp) {
		super(fp);
	}

	public boolean canCopy(ICopyContext context) {
		final PictogramElement[] pictogramElements = context.getPictogramElements();
		return pictogramElements != null && pictogramElements.length > 0;
	}

	public void copy(ICopyContext context) {
		final PictogramElement[] pes = context.getPictogramElements();
		putToClipboard(pes);
	}

	@Override
	public boolean hasDoneChanges() {
		return false;
	}
}