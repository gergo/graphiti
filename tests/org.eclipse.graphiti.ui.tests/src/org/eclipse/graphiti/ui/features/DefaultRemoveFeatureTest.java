/*********************************************************************
* Copyright (c) 2014, 2022 SAP SE
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Contributors:
*    mwenz - Bug 453553 - Provide an abort possibility for delete and remove features in case 'pre' methods fail
*    mwenz - Bug 580392 - Replace EasyMock with Mockito
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipse.graphiti.ui.features;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.IRemoveContext;
import org.eclipse.graphiti.features.impl.DefaultRemoveFeature;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.junit.Test;

public class DefaultRemoveFeatureTest {

	@Test
	public void testIsAbortAfterPreDelete() {
		IContext context = mock(IRemoveContext.class);
		IFeatureProvider featureProvider = mock(IFeatureProvider.class);

		IFeature feature = new TestRemoveFeatureAbort(featureProvider);

		try {
			feature.execute(context);
			fail("Must not be reached");
		} catch (OperationCanceledException e) {
			// expected
		}
	}

	private class TestRemoveFeatureAbort extends DefaultRemoveFeature {

		public TestRemoveFeatureAbort(IFeatureProvider fp) {
			super(fp);
		}

		@Override
		public boolean canRemove(IRemoveContext context) {
			return true;
		}

		@Override
		public boolean isRemoveAbort() {
			return true;
		}

		@Override
		protected void removeAllConnections(Anchor anchor) {
			fail("Must not be reached");
		}

		@Override
		public void remove(IRemoveContext context) {
			super.remove(context);
		}
	}
}
