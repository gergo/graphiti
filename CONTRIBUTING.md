Contributing to Graphiti
===================

Thanks for your interest in this project.

Project description:
--------------------

The goal of Graphiti is to support the fast and easy creation of graphical tools, which can display and edit underlying domain models using a tool-defined graphical notation.
Graphiti supports the developer in getting to a first version of an editor with very low effort by:
* Hiding platform specific technology (e.g. GEF / Draw2D on Eclipse)
* Providing rich default implementations inside the framework
* Providing a default look and feel that was designed in close co-operation with usability specialists

Link to Eclipse project page:
- [https://projects.eclipse.org/projects/modeling.gmp.graphiti](https://projects.eclipse.org/projects/modeling.gmp.graphiti)

Development Environment Setup:
------------------------------

You can set up a pre-configured IDE for the development of the Eclipse Communications Framework using the following link:

[![Create Eclipse Development Environment for Graphiti](https://download.eclipse.org/oomph/www/setups/svg/Graphiti.svg)](https://www.eclipse.org/setups/installer/?url=https://gitlab.eclipse.org/eclipse/graphiti/graphiti/-/raw/master/parent/GraphitiConfiguration.setup&show=true "Click to open Eclipse-Installer Auto Launch or drag onto your running installer's title area")


Developer resources:
--------------------

Information regarding source code management, builds, coding standards, and
more.
- [https://projects.eclipse.org/projects/modeling.graphiti/developer](https://projects.eclipse.org/projects/modeling.graphiti/developer)

The project maintains the following source code repositories
- [https://gitlab.eclipse.org/eclipse/graphiti/graphiti/](https://gitlab.eclipse.org/eclipse/graphiti/graphiti/)

This project uses Issues to track ongoing development and issues.
- [https://gitlab.eclipse.org/eclipse/graphiti/graphiti/-/issues](https://gitlab.eclipse.org/eclipse/graphiti/graphiti/-/issues)

Be sure to search for existing bugs issues you create another one.
Remember that contributions are always welcome!

Eclipse Development Process:
------------------------------

This Eclipse Foundation open project is governed by the Eclipse Foundation
Development Process and operates under the terms of the Eclipse IP Policy.

Eclipse Contributor Agreement:
------------------------------

Before your contribution can be accepted by the project team contributors must
electronically sign the Eclipse Contributor Agreement (ECA).

* [http://www.eclipse.org/legal/ECA.php](http://www.eclipse.org/legal/ECA.php)

Commits that are provided by non-committers must have a Signed-off-by field in
the footer indicating that the author is aware of the terms by which the
contribution has been provided to the project. The non-committer must
additionally have an Eclipse Foundation account and must have a signed Eclipse
Contributor Agreement (ECA) on file.

For more information, please see the Eclipse Committer Handbook:
   [https://www.eclipse.org/projects/handbook/#resources-commit](https://www.eclipse.org/projects/handbook/#resources-commit)

Contact:
--------

Contact the project developers via the project's "dev" list.

* [https://dev.eclipse.org/mailman/listinfo/graphiti-dev](https://dev.eclipse.org/mailman/listinfo/graphiti-dev)
* [mailto:graphiti-dev@eclipse.org](mailto:graphiti-dev@eclipse.org)

Contributions:
--------------

Contributions to the Eclipse Graphiti project are most welcome.
There are many ways to contribute, 
from entering high quality bug reports, to contributing code or documentation changes. 
For a complete guide, see the [Contributions and Committing] [1] page on the team wiki.

[1]: [https://gitlab.eclipse.org/eclipse/graphiti/graphiti/-/wikis/Graphiti](https://gitlab.eclipse.org/eclipse/graphiti/graphiti/-/wikis/Graphiti)

